import os

def read_expenses_from_file(filename="expenses.txt"):
    filepath = os.path.join(os.path.dirname(__file__), filename)
    
    with open(filepath, 'r') as file:
        return [int(line.strip()) for line in file]


       


def find_two_numbers_that_sum_to_target(expenses, target=2020):
    seen_numbers = set()

    for number in expenses:
        complement = target - number
        if complement in seen_numbers:
            return number, complement
        seen_numbers.add(number)

    return None, None

def product_of_two_numbers(filename="expenses.txt", target=2020):
    expenses = read_expenses_from_file(filename)
    number1, number2 = find_two_numbers_that_sum_to_target(expenses, target)
    if number1 and number2:
        return number1 * number2
    return None

def main():
    filename = "expenses.txt" 
    result = product_of_two_numbers(filename)
    if result:
        print(f"The product of the two numbers from '{filename}' that sum to 2020 is: {result}")
    else:
        print(f"No two numbers from '{filename}' sum up to 2020.")

if __name__ == "__main__":
    main()

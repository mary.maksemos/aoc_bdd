import os

def is_password_valid(policy, letter, password):
    pos1, pos2 = map(int, policy.split('-'))
    return (password[pos1-1] == letter) ^ (password[pos2-1] == letter)  

def valid_passwords_count(filename):
    with open(filename, 'r') as file:
        lines = file.readlines()

    valid_count = 0

    for line in lines:
        policy, letter_with_colon, password = line.strip().split()
        letter = letter_with_colon[0]
        
        if is_password_valid(policy, letter, password):
            valid_count += 1

    return valid_count

if __name__ == "__main__":
    print(valid_passwords_count("passwords.txt"))

import os

def valid_passwords_count(filename):
    with open(filename, 'r') as file:
        valid_count = 0
        for line in file:
            policy, letter, password = line.split(' ')
            
            min_occurrence, max_occurrence = map(int, policy.split('-'))
            
            letter = letter[0]
            
            occurrence = password.count(letter)
            
            if min_occurrence <= occurrence <= max_occurrence:
                valid_count += 1
    return valid_count

def main():
    filename = "passwords.txt"
    count = valid_passwords_count(filename)
    print(f"Number of valid passwords: {count}")

if __name__ == "__main__":
    main()

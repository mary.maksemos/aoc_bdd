import os
import day_1_part_1


def read_expenses_from_file(filename="expenses.txt"):
    filepath = os.path.join(os.path.dirname(__file__), filename)
    
    with open(filepath, 'r') as file:
        return [int(line.strip()) for line in file]

        
def find_three_numbers_that_sum_to_target(expenses, target=2020):

    expenses.sort()
    n = len(expenses)

    for i in range(n):
        for j in range(i+1, n):
            for k in range(j+1, n):
                if expenses[i] + expenses[j] + expenses[k] == target:
                    return expenses[i], expenses[j], expenses[k]

    return None, None, None

def product_of_three_numbers(filename="expenses.txt", target=2020):
    expenses = read_expenses_from_file(filename)
    number1, number2, number3 = find_three_numbers_that_sum_to_target(expenses, target)
    if number1 and number2 and number3:
        return number1 * number2 * number3
    return None

def main():
    filename = "expenses.txt"
    result = product_of_three_numbers(filename)
    if result:
        print(f"The product of the three numbers from '{filename}' that sum to 2020 is: {result}")
    else:
        print(f"No three numbers from '{filename}' sum up to 2020.")

if __name__ == "__main__":
    main()

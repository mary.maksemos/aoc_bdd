import pytest
from pytest_bdd import scenario, given, when, then
from day_2_part_1 import valid_passwords_count as valid_passwords_count_p1
from day_2_part_2 import valid_passwords_count as valid_passwords_count_p2

@scenario('features/password_validation.feature', 'Validate passwords from a file')
def test_password_validation_v1():
    pass

@scenario('features/password_validation_p2.feature', 'Validate passwords from a file based on new policy')
def test_password_validation_v2():
    pass

@pytest.fixture
def password_data():
    return {'filename': None, 'count': 0, 'version': 1}

@given('I have a passwords file named "passwords.txt"')
def set_file(password_data):
    password_data['filename'] = "passwords.txt"

@when('I validate passwords according to the policy')
def validate_passwords_v1(password_data):
    password_data['count'] = valid_passwords_count_p1(password_data['filename'])

@when('I validate passwords according to the new policy')
def validate_passwords_v2(password_data):
    password_data['count'] = valid_passwords_count_p2(password_data['filename'])

@then('I should get the count of valid passwords')
def get_valid_count(password_data):
    assert password_data['count'] > 0

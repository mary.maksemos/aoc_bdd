import pytest
from pytest_bdd import scenarios, given, when, then
from day_1_part_1 import product_of_two_numbers  
from day_1_part_2 import product_of_three_numbers

EXPECTED_PRODUCT1 = 542619
EXPECTED_PRODUCT2 = 32858450


scenarios('features/find_numbers.feature')
scenarios('features/find-three-numbers.feature')

@pytest.fixture
def expenses_data():
    return {'filename': None, 'product': None}

@given('I have an expense report "expenses.txt"')
def load_expenses(expenses_data):
    expenses_data['filename'] = "expenses.txt"

@when('I find two numbers in the file that sum up to 2020')
def find_product(expenses_data):
    expenses_data['product'] = product_of_two_numbers(expenses_data['filename'])

@then('I should get the product of these two numbers')
def check_product(expenses_data):
    assert expenses_data['product'] == EXPECTED_PRODUCT1

@when('I look for three numbers that sum to 2020')
def find_three_numbers(expenses_data):
    expenses_data['product'] = product_of_three_numbers(expenses_data['filename'])

@then('the product of those numbers should be 32858450')
def check_product_3_numbers(expenses_data):
    assert expenses_data['product'] == EXPECTED_PRODUCT2

Feature: New Password validation according to Toboggan Corporate Policy

  Scenario: Validate passwords from a file based on new policy
     Given I have a passwords file named "passwords.txt"
     When I validate passwords according to the new policy
     Then I should get the count of valid passwords

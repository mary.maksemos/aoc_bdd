Feature: Find two numbers that sum up to 2020 and get their product

  Scenario: Reading numbers from a file and finding their product
     Given I have an expense report "expenses.txt"
     When I find two numbers in the file that sum up to 2020
     Then I should get the product of these two numbers

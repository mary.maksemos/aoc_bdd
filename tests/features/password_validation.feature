Feature: Password validation according to policy

  Scenario: Validate passwords from a file
     Given I have a passwords file named "passwords.txt"
     When I validate passwords according to the policy
     Then I should get the count of valid passwords

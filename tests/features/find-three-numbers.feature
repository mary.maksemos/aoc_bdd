Feature: Finding three numbers that sum to 2020

    Scenario: Finding three numbers that sum to 2020
        Given I have an expense report "expenses.txt"
        When I look for three numbers that sum to 2020
        Then the product of those numbers should be 32858450


[![pipeline status](https://gitlab.com/mary.maksemos/aoc_bdd/badges/main/pipeline.svg)](https://gitlab.com/mary.maksemos/aoc_bdd/-/commits/main)

[![coverage report](https://gitlab.com/mary.maksemos/aoc_bdd/badges/main/coverage.svg)](https://gitlab.com/mary.maksemos/aoc_bdd/-/commits/main)

Check the detailed [coverage report xml format](https://mary.maksemos.gitlab.io/-/aoc_bdd/-/jobs/5254045596/artifacts/coverage.xml) 


 Check the detailed [coverage report html format](https://mary.maksemos.gitlab.io/-/aoc_bdd/-/jobs/5254045596/artifacts/coverage_html/index.html)



## Reflections about your experience using TDD vs BDD:

 ### TDD (Test-Driven Development):

Imagine: You're building a puzzle. Before adding each piece, you check if it fits perfectly.

Good Parts:

Helps you avoid mistakes because you're always double-checking.
Feels organized.
Not-So-Good Parts:

Can be a bit slow.
You might get too caught up in small details.


 ### BDD (Behavior-Driven Development):

Imagine: Before building the puzzle, you dream about the whole picture. You think of the stories behind it.

Good Parts:

Everyone gives their ideas, making it a team effort.
Focuses on the big picture and stories.
Not-So-Good Parts:

Can feel a bit dreamy at first.
Needs everyone to talk and understand each other.

My Feelings:

TDD is like talking to a computer. It's about getting things right.
BDD is like telling a story. It's about making something fun and useful.
Using both is like baking a cake with the right ingredients and making sure it tastes good too!

In short, TDD and BDD are like two different tools in a toolbox. Both are helpful in their own ways.